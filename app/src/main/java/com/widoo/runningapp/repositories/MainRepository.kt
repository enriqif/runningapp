package com.widoo.runningapp.repositories

import com.widoo.runningapp.db.Run
import com.widoo.runningapp.db.RunDAO
import javax.inject.Inject

class MainRepository @Inject constructor(
    val runDao: RunDAO
) {
    suspend fun insertRun(run:Run) = runDao.insertRun(run)
    suspend fun deleteRun(run:Run) = runDao.deleteRun(run)

    fun getAllRunsSortedByDate(run:Run) = runDao.getAllRunsSortedByDate()

    fun getAllRunsSortedByDistance(run:Run) = runDao.getAllRunsSortedByDistance()

    fun getAllRunsSortedByTimeMilliSeconds(run:Run) = runDao.getAllRunsSortedByTimeMilliSeconds()

    fun getAllRunsSortedByAvgSpeed(run:Run) = runDao.getAllRunsSortedByAvgSpeed()

    fun getAllRunsSortedByCaloriesBurned(run:Run) = runDao.getAllRunsSortedByCaloriesBurned()

    fun getTotalAvgSpeed() = runDao.getTotalAVGSpeed()

    fun getTotalDistance() = runDao.getTotalDistance()

    fun getTotalCaloriesBurned() = runDao.getTotalCaloriesBurned()

    fun getTotalTimeMilliSeconds() = runDao.getTotalTimeInMilliSeconds()
}